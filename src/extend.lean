import topology.metric_space.basic

noncomputable theory

open_locale topological_space
open filter set

variables {X Y : Type*} [topological_space X] [topological_space Y]

--local notation `𝓝[`s`] ` x := nhds_within x s

/-- Extend a function from a set `A`. At any `x₀`, if `f` converges to some `y` when `x` tends
to `x₀` within `A` then pick such a `y` as the new value, otherwise pick a junk value. -/
def extend_from (A : set X) (f : X → Y) : X → Y :=
λ x, @@lim _ ⟨f x⟩ (nhds_within x A) f

-- precise version of the promise of the previous docstring
lemma tendsto_extend_from {A : set X} {f : X → Y} {x : X}
  (h : ∃ y, tendsto f (nhds_within x A) (𝓝 y)) : tendsto f (nhds_within x A) (𝓝 $ extend_from A f x) :=
lim_spec h

lemma extend_from_eq [t2_space Y] {A : set X} {f : X → Y} {x : X} {y : Y} (hx : x ∈ closure A)
  (hf : tendsto f (nhds_within x A) (𝓝 y)) : extend_from A f x = y :=
begin
  haveI := mem_closure_iff_nhds_within_ne_bot.mp hx,
  exact tendsto_nhds_unique (lim_spec ⟨y, hf⟩) hf,
end

lemma extend_from_extends [t2_space Y] {f : X → Y} {A : set X} (hf : continuous_on f A) :
  ∀ x ∈ A, extend_from A f x = f x :=
λ x x_in, extend_from_eq (subset_closure x_in) (hf x x_in)

lemma continuous_on_extend_from [regular_space Y] {f : X → Y} {A B : set X} (hB : B ⊆ closure A)
  (hf : ∀ x ∈ B,  ∃ y, tendsto f (nhds_within x A) (𝓝 y)) : continuous_on (extend_from A f) B :=
begin
  set φ := extend_from A f,
  intros x x_in,
  suffices : ∀ V' ∈ 𝓝 (φ x), is_closed V' → φ ⁻¹' V' ∈ nhds_within x B,
    by simpa [continuous_within_at, (closed_nhds_basis _).tendsto_right_iff],
  intros V' V'_in V'_closed,
  obtain ⟨V, V_in, V_op, hV⟩ : ∃ V ∈ 𝓝 x, is_open V ∧ V ∩ A ⊆ f ⁻¹' V',
  { have := tendsto_extend_from (hf x  x_in),
    rcases (nhds_within_basis_open x A).tendsto_left_iff.mp this V' V'_in with ⟨V, ⟨hxV, V_op⟩, hV⟩,
    use [V, mem_nhds_sets V_op hxV, V_op, hV] },
  suffices : ∀ y ∈ V ∩ B, φ y ∈ V',
    from mem_sets_of_superset (inter_mem_inf_sets V_in $ mem_principal_self B) this,
  rintros y ⟨hyV, hyB⟩,
  haveI := mem_closure_iff_nhds_within_ne_bot.mp (hB hyB),
  have limy : tendsto f (nhds_within y A) (𝓝 $ φ y) := tendsto_extend_from (hf y hyB),
  have hVy : V ∈ 𝓝 y := mem_nhds_sets V_op hyV,
  have : V ∩ A ∈ (nhds_within y A),
    by simpa [inter_comm] using inter_mem_nhds_within _ hVy,
  exact mem_of_closed_of_tendsto limy V'_closed (mem_sets_of_superset this hV)
end

lemma continuous_extend_from [regular_space Y] {f : X → Y} {A : set X} (hA : ∀ x, x ∈ closure A)
  (hf : ∀ x,  ∃ y, tendsto f (nhds_within x A) (𝓝 y)) : continuous (extend_from A f) :=
begin
  rw continuous_iff_continuous_on_univ,
  exact continuous_on_extend_from (λ x _, hA x) (by simpa using hf)
end

lemma continuous_on_Icc_of_extend_continuous_on_Ioo {α β : Type*} [topological_space α]
  [linear_order α] [densely_ordered α] [order_topology α] [topological_space β] [t2_space β]
  [regular_space β] {f : α → β} {a b : α} {la lb : β} (hab : a < b) (hf : continuous_on f (Ioo a b))
  (ha : tendsto f (nhds_within a $ Ioi a) (𝓝 la)) (hb : tendsto f (nhds_within b $ Iio b) (𝓝 lb)) :
  continuous_on (extend_from (Ioo a b) f) (Icc a b) ∧
  (extend_from (Ioo a b) f a = la) ∧
  (extend_from (Ioo a b) f b = lb) ∧
  ∀ x ∈ Ioo a b, extend_from (Ioo a b) f x = f x :=
begin
  split,
  { apply continuous_on_extend_from,
    rw closure_Ioo hab,
    intros x x_in,
    have : a = x ∨ b = x ∨ x ∈ Ioo a b, 
    { rw [mem_Icc, le_iff_lt_or_eq, le_iff_lt_or_eq] at x_in,
      exact x_in.1.elim
        (λ h, x_in.2.elim
          (or.inr ∘ or.inr ∘ and.intro h)
          (or.inr ∘ or.inl ∘ symm))
        or.inl },
    rcases this with rfl | rfl | h,
    { use la,
      simpa [hab] },
    { use lb,
      simpa [hab] },
    { use [f x, hf x h] },
  },
  repeat
  { split,
    apply extend_from_eq,
    rw closure_Ioo hab,
    simp only [le_of_lt hab, left_mem_Icc, right_mem_Icc],
    simpa [hab] },
  { exact extend_from_extends hf }
end

lemma continuous_on_Ico_of_extend_continuous_on_Ioo {α β : Type*} [topological_space α]
  [linear_order α] [densely_ordered α] [order_topology α] [topological_space β] [t2_space β]
  [regular_space β] {f : α → β} {a b : α} {la : β} (hab : a < b) (hf : continuous_on f (Ioo a b))
  (ha : tendsto f (nhds_within a $ Ioi a) (𝓝 la)) :
  continuous_on (extend_from (Ioo a b) f) (Ico a b) ∧
  (extend_from (Ioo a b) f a = la) ∧
  ∀ x ∈ Ioo a b, extend_from (Ioo a b) f x = f x :=
begin
  split,
  { apply continuous_on_extend_from,
    rw [closure_Ioo hab], exact Ico_subset_Icc_self,
    intros x x_in,
    have : a = x ∨ x ∈ Ioo a b, 
    { rw [mem_Ico, le_iff_lt_or_eq] at x_in,
      exact x_in.1.elim
        (or.inr ∘ (λ h, and.intro h x_in.2))
        or.inl },
    rcases this with rfl | h,
    { use la,
      simpa [hab] },
    { use [f x, hf x h] },
  },
  split,
  { apply extend_from_eq,
    rw closure_Ioo hab,
    simp only [le_of_lt hab, left_mem_Icc],
    simpa [hab] },
  { exact extend_from_extends hf }
end

lemma continuous_on_Ioc_of_extend_continuous_on_Ioo {α β : Type*} [topological_space α]
  [linear_order α] [densely_ordered α] [order_topology α] [topological_space β] [t2_space β]
  [regular_space β] {f : α → β} {a b : α} {lb : β} (hab : a < b) (hf : continuous_on f (Ioo a b))
  (hb : tendsto f (nhds_within b $ Iio b) (𝓝 lb)) :
  continuous_on (extend_from (Ioo a b) f) (Ioc a b) ∧
  (extend_from (Ioo a b) f b = lb) ∧
  ∀ x ∈ Ioo a b, extend_from (Ioo a b) f x = f x :=
begin
  have := @continuous_on_Ico_of_extend_continuous_on_Ioo (order_dual α) _ _ _ _ _ _ _ _ f _ _ _ hab,
  erw [dual_Ico, dual_Ioi, dual_Ioo] at this,
  exact this hf hb
end