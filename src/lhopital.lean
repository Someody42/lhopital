import analysis.calculus.deriv
import analysis.calculus.mean_value
import tactic.squeeze
import tactic.interval_cases

import lemmas

open filter set

open_locale filter topological_space

attribute [instance] classical.prop_decidable

variables {a b : ℝ} (hab : a < b) {l : filter ℝ} {f f' g g' : ℝ → ℝ} 

include hab

theorem has_deriv_at.lhopital_zero_right_on_Ioo
  (hff' : ∀ x ∈ Ioo a b, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Ioo a b, has_deriv_at g (g' x) x)
  (hg' : ∀ x ∈ Ioo a b, g' x ≠ 0)
  (hfa : tendsto f (𝓝[Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l :=
begin
  have sub : ∀ x ∈ Ioo a b, Ioo a x ⊆ Ioo a b := λ x hx, Ioo_subset_Ioo (le_refl a) (le_of_lt hx.2),
  have hg : ∀ x ∈ (Ioo a b), g x ≠ 0,
  { intros x hx h,
    have : tendsto g (𝓝[Iio x] x) (𝓝 0),
    { rw [← h, ← nhds_within_Ioo_eq_nhds_within_Iio hx.1], 
      exact ((hgg' x hx).continuous_at.continuous_within_at.mono $ sub x hx).tendsto },
    obtain ⟨y, hyx, hy⟩ : ∃ c ∈ Ioo a x, g' c = 0,
      from exists_has_deriv_at_eq_zero' hx.1 hga this (λ y hy, hgg' y $ sub x hx hy),
    exact hg' y (sub x hx hyx) hy },
  /- The proof would be significantly more complex if we used `∀ x ∈ Ioo a b, ∃ c, ...`, because
    `choose c hc using this` would give `c : Π x, x ∈ Ioo a b → ℝ` instead of `c : ℝ → ℝ`. -/
  have : ∀ x, ∃ c, ∀ (h : x ∈ Ioo a b), (c ∈ Ioo a x ∧ (f x) * (g' c) = (g x) * (f' c)),
  { intro x,
    refine classical.by_cases _ (λ hx : ¬x ∈ Ioo a b, ⟨0, hx.elim⟩),
    intros hx,
    obtain ⟨c, hca, hcb⟩ : ∃ (c : ℝ) (H : c ∈ Ioo a x), (f x) * g' c = (g x) * f' c,
    { rw [← sub_zero (f x), ← sub_zero (g x)],
      exact exists_ratio_has_deriv_at_eq_ratio_slope' g g' hx.1 f f'
        (λ y hy, hgg' y $ sub x hx hy) (λ y hy, hff' y $ sub x hx hy) hga hfa 
        (tendsto_nhds_within_of_tendsto_nhds (hgg' x hx).continuous_at.tendsto)
        (tendsto_nhds_within_of_tendsto_nhds (hff' x hx).continuous_at.tendsto) },
    exact ⟨c, λ _, ⟨hca, hcb⟩⟩ },
  choose c hc using this,
  have : ∀ x ∈ Ioo a b, ((λ x', (f' x') / (g' x')) ∘ c) x = f x / g x, 
  { intros x hx,
    rcases hc x hx with ⟨h₁, h₂⟩,
    field_simp [hg x hx, hg' (c x) ((sub x hx) h₁)],
    simp only [h₂],
    rwa mul_comm },
  have cmp : ∀ x ∈ Ioo a b, a < c x ∧ c x < x,
    from λ x hx, (hc x hx).1,
  rw ← nhds_within_Ioo_eq_nhds_within_Ioi hab,
  apply tendsto_nhds_within_congr this,
  simp only,
  apply hdiv.comp,
  refine tendsto_nhds_within_of_tendsto_nhds_of_eventually_within _
    (tendsto_of_tendsto_of_tendsto_of_le_of_le' tendsto_const_nhds 
      (tendsto_nhds_within_of_tendsto_nhds tendsto_id) _ _) _,
  all_goals 
  { apply eventually_nhds_with_of_forall,
    intros x hx,
    have := cmp x hx,
    try {simp}, 
    linarith [this] }
end

theorem deriv.lhopital_zero_right_on_Ioo
  (hdf : differentiable_on ℝ f (Ioo a b)) (hg' : ∀ x ∈ Ioo a b, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l :=
begin
  have hdf : ∀ x ∈ Ioo a b, differentiable_at ℝ f x,
    from λ x hx, (hdf x hx).differentiable_at (Ioo_mem_nhds hx.1 hx.2),
  have hdg : ∀ x ∈ Ioo a b, differentiable_at ℝ g x,
    from λ x hx, decidable.by_contradiction (λ h, hg' x hx (deriv_zero_of_not_differentiable_at h)),
  exact has_deriv_at.lhopital_zero_right_on_Ioo hab (λ x hx, (hdf x hx).has_deriv_at) 
    (λ x hx, (hdg x hx).has_deriv_at) hg' hfa hga hdiv
end

theorem has_deriv_at.lhopital_zero_right_on_Ico 
  (hff' : ∀ x ∈ Ioo a b, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Ioo a b, has_deriv_at g (g' x) x)
  (hcf : continuous_on f (Ico a b)) (hcg : continuous_on g (Ico a b))
  (hg' : ∀ x ∈ Ioo a b, g' x ≠ 0)
  (hfa : f a = 0) (hga : g a = 0)
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (nhds_within a (Ioi a)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within a (Ioi a)) l :=
begin
  refine has_deriv_at.lhopital_zero_right_on_Ioo hab hff' hgg' hg' _ _ hdiv, 
  { rw [← hfa, ← nhds_within_Ioo_eq_nhds_within_Ioi hab],
    exact ((hcf a $ left_mem_Ico.mpr hab).mono Ioo_subset_Ico_self).tendsto },
  { rw [← hga, ← nhds_within_Ioo_eq_nhds_within_Ioi hab],
    exact ((hcg a $ left_mem_Ico.mpr hab).mono Ioo_subset_Ico_self).tendsto },
end

theorem deriv.lhopital_zero_right_on_Ico 
  (hdf : differentiable_on ℝ f (Ioo a b))
  (hcf : continuous_on f (Ico a b)) (hcg : continuous_on g (Ico a b))
  (hg' : ∀ x ∈ (Ioo a b), (deriv g) x ≠ 0)
  (hfa : f a = 0) (hga : g a = 0)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within a (Ioi a)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within a (Ioi a)) l :=
begin
  refine deriv.lhopital_zero_right_on_Ioo hab hdf hg' _ _ hdiv, 
  { rw [← hfa, ← nhds_within_Ioo_eq_nhds_within_Ioi hab],
    exact ((hcf a $ left_mem_Ico.mpr hab).mono Ioo_subset_Ico_self).tendsto },
  { rw [← hga, ← nhds_within_Ioo_eq_nhds_within_Ioi hab],
    exact ((hcg a $ left_mem_Ico.mpr hab).mono Ioo_subset_Ico_self).tendsto },
end

-- TODO : refactor
/-
theorem lhopital_zero_of_mem_nhds_right {a : ℝ} {s : set ℝ} (hs : s ∈ 𝓝[Ioi a] a) {f g : ℝ → ℝ}
  {l : filter ℝ} (hdf : differentiable_on ℝ f $ s ∩ Ioi a)
  (hg' : ∀ x ∈ s ∩ Ioi a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[s ∩ Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[s ∩ Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[s ∩ Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l := 
begin
  rw mem_nhds_within_Ioi_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨u, hau, hu⟩,
  have : Ioo a u ⊆ s ∩ Ioi a := λ x hx, mem_inter (hu hx) hx.1,
  apply lhopital_zero_at_left_of_Ioo hau f g (hdf.mono this) (λ x hx, hg' x ⟨hu hx, hx.1⟩);
  rw ← nhds_within_Ioo_eq_nhds_within_Ioi hau;
  apply tendsto_nhds_within_mono_left this;
  assumption
end

theorem lhopital_zero_of_mem_nhds_right' {a : ℝ} {s : set ℝ} (hs : s ∈ 𝓝[Ioi a] a) {f g : ℝ → ℝ}
  {l : filter ℝ} (hdf : differentiable_on ℝ f s)
  (hg' : ∀ x ∈ s, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[s] a) (𝓝 0)) (hga : tendsto g (𝓝[s] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l := 
begin
  rw mem_nhds_within_Ioi_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨u, hau, hu⟩,
  apply lhopital_zero_at_left_of_Ioo hau f g (hdf.mono hu) (λ x hx, hg' x $ hu hx) _ _ hdiv;
  rw ← nhds_within_Ioo_eq_nhds_within_Ioi hau;
  apply tendsto_nhds_within_mono_left hu;
  assumption
end

-- TODO : think
theorem lhopital_zero_at_left_of_Ico' {a b : ℝ} (hab : a < b) (f g : ℝ → ℝ) {l : filter ℝ} 
  (hdf : differentiable_on ℝ f (Ioo a b))
  (hcf : continuous_on f (Ico a b)) (hcg : continuous_on g (Ico a b))
  (hg' : ∀ x ∈ (Ioo a b), (deriv g) x ≠ 0)
  (hfa : f a = 0) (hga : g a = 0)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within a (Ioi a)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within a (Ioi a)) l :=
begin
  have hcf' := tendsto_nhds_within_mono_left Ioo_subset_Ico_self (hcf a ⟨le_refl _, hab⟩),
  have hcg' := tendsto_nhds_within_mono_left Ioo_subset_Ico_self (hcg a ⟨le_refl _, hab⟩),
  rw [hfa, nhds_within_Ioo_eq_nhds_within_Ioi hab] at hcf',
  rw [hga, nhds_within_Ioo_eq_nhds_within_Ioi hab] at hcg',
  refine lhopital_zero_right _ _ hcf' hcg' hdiv;
  rw eventually_iff_exists_mem;
  use [Ioo a b, Ioo_mem_nhds_within_Ioi ⟨le_refl _, hab⟩],
  { exact λ x hx, (hdf x hx).differentiable_at (Ioo_mem_nhds hx.1 hx.2) },
  { exact hg' }
end
-/
-- END refactor section

theorem has_deriv_at.lhopital_zero_left_on_Ioo
  (hff' : ∀ x ∈ Ioo a b, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Ioo a b, has_deriv_at g (g' x) x)
  (hg' : ∀ x ∈ Ioo a b, g' x ≠ 0)
  (hfb : tendsto f (nhds_within b (Iio b)) (𝓝 0)) (hgb : tendsto g (nhds_within b (Iio b)) (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (nhds_within b (Iio b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b)) l :=
begin
  -- Here, we essentially compose by `has_neg.neg`. The following is mostly technical details.
  have hdnf : ∀ x ∈ -Ioo a b, has_deriv_at (f ∘ has_neg.neg) (f' (-x) * (-1)) x,
    from λ x hx, has_deriv_at.comp x (hff' (-x) hx) (has_deriv_at_neg x),
  have hdng : ∀ x ∈ -Ioo a b, has_deriv_at (g ∘ has_neg.neg) (g' (-x) * (-1)) x,
    from λ x hx, has_deriv_at.comp x (hgg' (-x) hx) (has_deriv_at_neg x),
  rw preimage_neg_Ioo at hdnf,
  rw preimage_neg_Ioo at hdng,
  have := has_deriv_at.lhopital_zero_right_on_Ioo (neg_lt_neg hab) hdnf hdng
    (by { intros x hx h,
          apply hg' _ (by {rw ← preimage_neg_Ioo at hx, exact hx}), 
          rwa [mul_comm, ← neg_eq_neg_one_mul, neg_eq_zero] at h })
    (hfb.comp tendsto_neg_nhds_within_Ioi_neg)
    (hgb.comp tendsto_neg_nhds_within_Ioi_neg)
    (by { simp only [neg_div_neg_eq, mul_one, mul_neg_eq_neg_mul_symm],
          exact (tendsto_congr $ λ x, rfl).mp (hdiv.comp tendsto_neg_nhds_within_Ioi_neg) }),
  have := this.comp tendsto_neg_nhds_within_Iio,
  unfold function.comp at this,
  simpa only [neg_neg]
end

theorem deriv.lhopital_zero_left_on_Ioo
  (hdf : differentiable_on ℝ f (Ioo a b))
  (hg' : ∀ x ∈ (Ioo a b), (deriv g) x ≠ 0)
  (hfb : tendsto f (nhds_within b (Iio b)) (𝓝 0)) (hgb : tendsto g (nhds_within b (Iio b)) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b (Iio b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b)) l :=
begin
  have hdf : ∀ x ∈ Ioo a b, differentiable_at ℝ f x,
    from λ x hx, (hdf x hx).differentiable_at (Ioo_mem_nhds hx.1 hx.2),
  have hdg : ∀ x ∈ Ioo a b, differentiable_at ℝ g x,
    from λ x hx, decidable.by_contradiction (λ h, hg' x hx (deriv_zero_of_not_differentiable_at h)),
  exact has_deriv_at.lhopital_zero_left_on_Ioo hab (λ x hx, (hdf x hx).has_deriv_at) 
    (λ x hx, (hdg x hx).has_deriv_at) hg' hfb hgb hdiv
end

theorem has_deriv_at.lhopital_zero_left_on_Ioc
  (hff' : ∀ x ∈ Ioo a b, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Ioo a b, has_deriv_at g (g' x) x)
  (hcf : continuous_on f (Ioc a b)) (hcg : continuous_on g (Ioc a b))
  (hg' : ∀ x ∈ Ioo a b, g' x ≠ 0)
  (hfb : f b = 0) (hgb : g b = 0)
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (nhds_within b (Iio b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b)) l :=
begin
  refine has_deriv_at.lhopital_zero_left_on_Ioo hab hff' hgg' hg' _ _ hdiv, 
  { rw [← hfb, ← nhds_within_Ioo_eq_nhds_within_Iio hab],
    exact ((hcf b $ right_mem_Ioc.mpr hab).mono Ioo_subset_Ioc_self).tendsto },
  { rw [← hgb, ← nhds_within_Ioo_eq_nhds_within_Iio hab],
    exact ((hcg b $ right_mem_Ioc.mpr hab).mono Ioo_subset_Ioc_self).tendsto },
end

theorem deriv.lhopital_zero_left_on_Ioc
  (hdf : differentiable_on ℝ f (Ioo a b))
  (hcf : continuous_on f (Ioc a b)) (hcg : continuous_on g (Ioc a b))
  (hg' : ∀ x ∈ (Ioo a b), (deriv g) x ≠ 0)
  (hfb : f b = 0) (hgb : g b = 0)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b (Iio b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b)) l :=
begin
  refine deriv.lhopital_zero_left_on_Ioo hab hdf hg' _ _ hdiv,
  { rw [← hfb, ← nhds_within_Ioo_eq_nhds_within_Iio hab],
    exact ((hcf b $ right_mem_Ioc.mpr hab).mono Ioo_subset_Ioc_self).tendsto },
  { rw [← hgb, ← nhds_within_Ioo_eq_nhds_within_Iio hab],
    exact ((hcg b $ right_mem_Ioc.mpr hab).mono Ioo_subset_Ioc_self).tendsto }
end

omit hab

theorem has_deriv_at.lhopital_zero_at_top_on_Ioi
  (hff' : ∀ x ∈ Ioi a, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Ioi a, has_deriv_at g (g' x) x)
  (hg' : ∀ x ∈ Ioi a, g' x ≠ 0)
  (hftop : tendsto f at_top (𝓝 0)) (hgtop : tendsto g at_top (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) at_top l) :
  tendsto (λ x, (f x) / (g x)) at_top l :=
begin
  obtain ⟨ a', haa', ha'⟩ : ∃ a', a < a' ∧ 0 < a' :=
    ⟨1 + max a 0, ⟨lt_of_le_of_lt (le_max_left a 0) (lt_one_add _), 
                   lt_of_le_of_lt (le_max_right a 0) (lt_one_add _)⟩⟩,
  have fact1 : ∀ (x:ℝ), x ∈ Ioo 0 a'⁻¹ → x ≠ 0 := λ _ hx, (ne_of_lt hx.1).symm,
  have fact2 : ∀ x ∈ Ioo 0 a'⁻¹, a < x⁻¹,
    from λ _ hx, lt_trans haa' ((lt_inv ha' hx.1).mpr hx.2),
  have hdnf : ∀ x ∈ Ioo 0 a'⁻¹, has_deriv_at (f ∘ has_inv.inv) (f' (x⁻¹) * (-(x^2)⁻¹)) x,
    from λ x hx, has_deriv_at.comp x (hff' (x⁻¹) $ fact2 x hx) (has_deriv_at_inv $ fact1 x hx),
  have hdng : ∀ x ∈ Ioo 0 a'⁻¹, has_deriv_at (g ∘ has_inv.inv) (g' (x⁻¹) * (-(x^2)⁻¹)) x,
    from λ x hx, has_deriv_at.comp x (hgg' (x⁻¹) $ fact2 x hx) (has_deriv_at_inv $ fact1 x hx),
  have := has_deriv_at.lhopital_zero_right_on_Ioo (inv_pos.mpr ha') hdnf hdng
    (by { intros x hx,
          refine mul_ne_zero _ (neg_ne_zero.mpr $ inv_ne_zero $ pow_ne_zero _ $ fact1 x hx),
          exact hg' _ (fact2 x hx) })
    (hftop.comp tendsto_inv_zero_at_top)
    (hgtop.comp tendsto_inv_zero_at_top)
    (by { refine (tendsto_congr' _).mp (hdiv.comp tendsto_inv_zero_at_top),
          rw eventually_eq_iff_exists_mem,
          use [Ioi 0, self_mem_nhds_within], 
          intros x hx,
          unfold function.comp,
          erw mul_div_mul_right,
          refine neg_ne_zero.mpr (inv_ne_zero $ pow_ne_zero _ $ ne_of_gt hx) }),
  have := this.comp tendsto_inv_at_top_zero',
  unfold function.comp at this,
  simpa only [inv_inv'],
end

theorem deriv.lhopital_zero_at_top_on_Ioi
  (hdf : differentiable_on ℝ f (Ioi a))
  (hg' : ∀ x ∈ (Ioi a), (deriv g) x ≠ 0)
  (hftop : tendsto f at_top (𝓝 0)) (hgtop : tendsto g at_top (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) at_top l) :
  tendsto (λ x, (f x) / (g x)) at_top l :=
begin
  have hdf : ∀ x ∈ Ioi a, differentiable_at ℝ f x,
    from λ x hx, (hdf x hx).differentiable_at (Ioi_mem_nhds hx),
  have hdg : ∀ x ∈ Ioi a, differentiable_at ℝ g x,
    from λ x hx, decidable.by_contradiction (λ h, hg' x hx (deriv_zero_of_not_differentiable_at h)),
  exact has_deriv_at.lhopital_zero_at_top_on_Ioi (λ x hx, (hdf x hx).has_deriv_at) 
    (λ x hx, (hdg x hx).has_deriv_at) hg' hftop hgtop hdiv,
end

theorem has_deriv_at.lhopital_zero_at_bot_on_Iio
  (hff' : ∀ x ∈ Iio a, has_deriv_at f (f' x) x) (hgg' : ∀ x ∈ Iio a, has_deriv_at g (g' x) x)
  (hg' : ∀ x ∈ Iio a, g' x ≠ 0)
  (hfbot : tendsto f at_bot (𝓝 0)) (hgbot : tendsto g at_bot (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) at_bot l) :
  tendsto (λ x, (f x) / (g x)) at_bot l :=
begin
  -- Here, we essentially compose by `has_neg.neg`. The following is mostly technical details.
  have hdnf : ∀ x ∈ -Iio a, has_deriv_at (f ∘ has_neg.neg) (f' (-x) * (-1)) x,
    from λ x hx, has_deriv_at.comp x (hff' (-x) hx) (has_deriv_at_neg x),
  have hdng : ∀ x ∈ -Iio a, has_deriv_at (g ∘ has_neg.neg) (g' (-x) * (-1)) x,
    from λ x hx, has_deriv_at.comp x (hgg' (-x) hx) (has_deriv_at_neg x),
  rw preimage_neg_Iio at hdnf,
  rw preimage_neg_Iio at hdng,
  have := has_deriv_at.lhopital_zero_at_top_on_Ioi hdnf hdng
    (by { intros x hx h,
          apply hg' _ (by {rw ← preimage_neg_Iio at hx, exact hx}), 
          rwa [mul_comm, ← neg_eq_neg_one_mul, neg_eq_zero] at h })
    (hfbot.comp tendsto_neg_at_top_at_bot)
    (hgbot.comp tendsto_neg_at_top_at_bot)
    (by { simp only [mul_one, mul_neg_eq_neg_mul_symm, neg_div_neg_eq],
          exact (tendsto_congr $ λ x, rfl).mp (hdiv.comp tendsto_neg_at_top_at_bot) }),
  have := this.comp tendsto_neg_at_bot_at_top,
  unfold function.comp at this,
  simpa only [neg_neg],
end

theorem deriv.lhopital_zero_at_bot_on_Iio
  (hdf : differentiable_on ℝ f (Iio a))
  (hg' : ∀ x ∈ (Iio a), (deriv g) x ≠ 0)
  (hfbot : tendsto f at_bot (𝓝 0)) (hgbot : tendsto g at_bot (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) at_bot l) :
  tendsto (λ x, (f x) / (g x)) at_bot l :=
begin
  have hdf : ∀ x ∈ Iio a, differentiable_at ℝ f x,
    from λ x hx, (hdf x hx).differentiable_at (Iio_mem_nhds hx),
  have hdg : ∀ x ∈ Iio a, differentiable_at ℝ g x,
    from λ x hx, decidable.by_contradiction (λ h, hg' x hx (deriv_zero_of_not_differentiable_at h)),
  exact has_deriv_at.lhopital_zero_at_bot_on_Iio (λ x hx, (hdf x hx).has_deriv_at) 
    (λ x hx, (hdg x hx).has_deriv_at) hg' hfbot hgbot hdiv,
end

-- TODO : refactor * from here : done (?)

theorem has_deriv_at.lhopital_zero_nhds_right
  (hff' : ∀ᶠ x in 𝓝[Ioi a] a, has_deriv_at f (f' x) x) 
  (hgg' : ∀ᶠ x in 𝓝[Ioi a] a, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in 𝓝[Ioi a] a, g' x ≠ 0)
  (hfa : tendsto f (𝓝[Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l :=
begin
  rw eventually_iff_exists_mem at *,
  rcases hff' with ⟨s₁, hs₁, hff'⟩,
  rcases hgg' with ⟨s₂, hs₂, hgg'⟩,
  rcases hg' with ⟨s₃, hs₃, hg'⟩,
  let s := s₁ ∩ s₂ ∩ s₃,
  have hs : s ∈ 𝓝[Ioi a] a := inter_mem_sets (inter_mem_sets hs₁ hs₂) hs₃,
  rw mem_nhds_within_Ioi_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨u, hau, hu⟩,
  refine has_deriv_at.lhopital_zero_right_on_Ioo hau _ _ _ hfa hga hdiv;
  intros x hx;
  apply_assumption;
  exact (hu hx).1.1 <|> exact (hu hx).1.2 <|> exact (hu hx).2
end

theorem deriv.lhopital_zero_nhds_right
  (hdf : ∀ᶠ x in 𝓝[Ioi a] a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝[Ioi a] a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l := 
begin
  have hdg : ∀ᶠ x in 𝓝[Ioi a] a, differentiable_at ℝ g x,
    from hg'.mp (eventually_of_forall $ 
      λ _ hg', decidable.by_contradiction (λ h, hg' (deriv_zero_of_not_differentiable_at h))),
  have hdf' : ∀ᶠ x in 𝓝[Ioi a] a, has_deriv_at f (deriv f x) x,
    from hdf.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  have hdg' : ∀ᶠ x in 𝓝[Ioi a] a, has_deriv_at g (deriv g x) x,
    from hdg.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  exact has_deriv_at.lhopital_zero_nhds_right hdf' hdg' hg' hfa hga hdiv
end

/-
theorem deriv.lhopital_zero_nhds_right
  (hdf : ∀ᶠ x in 𝓝[Ioi a] a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝[Ioi a] a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[Ioi a] a) (𝓝 0)) (hga : tendsto g (𝓝[Ioi a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Ioi a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Ioi a] a) l := 
begin
  rw eventually_iff_exists_mem at *,
  rcases hdf with ⟨s₁, hs₁, hdf⟩,
  rcases hg' with ⟨s₂, hs₂, hg'⟩,
  let s := s₁ ∩ s₂,
  have hs : s ∈ 𝓝[Ioi a] a := inter_mem_sets hs₁ hs₂,
  rw mem_nhds_within_Ioi_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨u, hau, hu⟩,
  refine deriv.lhopital_zero_right_on_Ioo hau _ (λ x hx, hg' x $ (hu hx).2) hfa hga hdiv,
  exact λ x hx, (hdf x $ (hu hx).1).differentiable_within_at
end
-/

theorem has_deriv_at.lhopital_zero_nhds_left
  (hff' : ∀ᶠ x in 𝓝[Iio a] a, has_deriv_at f (f' x) x) 
  (hgg' : ∀ᶠ x in 𝓝[Iio a] a, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in 𝓝[Iio a] a, g' x ≠ 0)
  (hfa : tendsto f (𝓝[Iio a] a) (𝓝 0)) (hga : tendsto g (𝓝[Iio a] a) (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (𝓝[Iio a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Iio a] a) l := 
begin
  rw eventually_iff_exists_mem at *,
  rcases hff' with ⟨s₁, hs₁, hff'⟩,
  rcases hgg' with ⟨s₂, hs₂, hgg'⟩,
  rcases hg' with ⟨s₃, hs₃, hg'⟩,
  let s := s₁ ∩ s₂ ∩ s₃,
  have hs : s ∈ 𝓝[Iio a] a := inter_mem_sets (inter_mem_sets hs₁ hs₂) hs₃,
  rw mem_nhds_within_Iio_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨l, hal, hl⟩,
  refine has_deriv_at.lhopital_zero_left_on_Ioo hal _ _ _ hfa hga hdiv;
  intros x hx;
  apply_assumption;
  exact (hl hx).1.1 <|> exact (hl hx).1.2 <|> exact (hl hx).2
end

theorem deriv.lhopital_zero_nhds_left
  (hdf : ∀ᶠ x in 𝓝[Iio a] a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝[Iio a] a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[Iio a] a) (𝓝 0)) (hga : tendsto g (𝓝[Iio a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Iio a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Iio a] a) l := 
begin
  have hdg : ∀ᶠ x in 𝓝[Iio a] a, differentiable_at ℝ g x,
    from hg'.mp (eventually_of_forall $ 
      λ _ hg', decidable.by_contradiction (λ h, hg' (deriv_zero_of_not_differentiable_at h))),
  have hdf' : ∀ᶠ x in 𝓝[Iio a] a, has_deriv_at f (deriv f x) x,
    from hdf.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  have hdg' : ∀ᶠ x in 𝓝[Iio a] a, has_deriv_at g (deriv g x) x,
    from hdg.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  exact has_deriv_at.lhopital_zero_nhds_left hdf' hdg' hg' hfa hga hdiv
end

/-
theorem deriv.lhopital_zero_nhds_left
  (hdf : ∀ᶠ x in 𝓝[Iio a] a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝[Iio a] a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[Iio a] a) (𝓝 0)) (hga : tendsto g (𝓝[Iio a] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[Iio a] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[Iio a] a) l := 
begin
  rw eventually_iff_exists_mem at *,
  rcases hdf with ⟨s₁, hs₁, hdf⟩,
  rcases hg' with ⟨s₂, hs₂, hg'⟩,
  let s := s₁ ∩ s₂,
  have hs : s ∈ 𝓝[Iio a] a := inter_mem_sets hs₁ hs₂,
  rw mem_nhds_within_Iio_iff_exists_Ioo_subset at hs,
  rcases hs with ⟨l, hal, hl⟩,
  refine deriv.lhopital_zero_left_on_Ioo hal _ (λ x hx, hg' x $ (hl hx).2) hfa hga hdiv,
  exact λ x hx, (hdf x $ (hl hx).1).differentiable_within_at
end
-/

theorem has_deriv_at.lhopital_zero_nhds'
  (hff' : ∀ᶠ x in 𝓝[univ \ {a}] a, has_deriv_at f (f' x) x)
  (hgg' : ∀ᶠ x in 𝓝[univ \ {a}] a, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in 𝓝[univ \ {a}] a, g' x ≠ 0)
  (hfa : tendsto f (𝓝[univ \ {a}] a) (𝓝 0)) (hga : tendsto g (𝓝[univ \ {a}] a) (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) (𝓝[univ \ {a}] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[univ \ {a}] a) l := 
begin
  have : univ \ {a} = Iio a ∪ Ioi a,
  { ext, rw [mem_diff_singleton, eq_true_intro $ mem_univ x, true_and, ne_iff_lt_or_gt], refl },
  simp only [this, nhds_within_union, tendsto_sup, eventually_sup] at *,
  exact ⟨has_deriv_at.lhopital_zero_nhds_left hff'.1 hgg'.1 hg'.1 hfa.1 hga.1 hdiv.1, 
          has_deriv_at.lhopital_zero_nhds_right hff'.2 hgg'.2 hg'.2 hfa.2 hga.2 hdiv.2⟩
end

theorem deriv.lhopital_zero_nhds'
  (hdf : ∀ᶠ x in 𝓝[univ \ {a}] a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝[univ \ {a}] a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝[univ \ {a}] a) (𝓝 0)) (hga : tendsto g (𝓝[univ \ {a}] a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝[univ \ {a}] a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[univ \ {a}] a) l := 
begin
  have : univ \ {a} = Iio a ∪ Ioi a,
  { ext, rw [mem_diff_singleton, eq_true_intro $ mem_univ x, true_and, ne_iff_lt_or_gt], refl },
  simp only [this, nhds_within_union, tendsto_sup, eventually_sup] at *,
  exact ⟨deriv.lhopital_zero_nhds_left hdf.1 hg'.1 hfa.1 hga.1 hdiv.1, 
          deriv.lhopital_zero_nhds_right hdf.2 hg'.2 hfa.2 hga.2 hdiv.2⟩,
end

theorem has_deriv_at.lhopital_zero_nhds 
  (hff' : ∀ᶠ x in 𝓝 a, has_deriv_at f (f' x) x)
  (hgg' : ∀ᶠ x in 𝓝 a, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in 𝓝 a, g' x ≠ 0)
  (hfa : tendsto f (𝓝 a) (𝓝 0)) (hga : tendsto g (𝓝 a) (𝓝 0))
  (hdiv : tendsto (λ x, f' x / g' x) (𝓝 a) l) :
  tendsto (λ x, f x / g x) (𝓝[univ \ {a}] a) l :=
begin
  apply @has_deriv_at.lhopital_zero_nhds' _ _ _ f' _ g';
  apply eventually_nhds_within_of_eventually_nhds <|> apply tendsto_nhds_within_of_tendsto_nhds;
  assumption
end

theorem deriv.lhopital_zero_nhds
  (hdf : ∀ᶠ x in 𝓝 a, differentiable_at ℝ f x)
  (hg' : ∀ᶠ x in 𝓝 a, deriv g x ≠ 0)
  (hfa : tendsto f (𝓝 a) (𝓝 0)) (hga : tendsto g (𝓝 a) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (𝓝 a) l) :
  tendsto (λ x, (f x) / (g x)) (𝓝[univ \ {a}] a) l := 
begin
  apply deriv.lhopital_zero_nhds';
  apply eventually_nhds_within_of_eventually_nhds <|> apply tendsto_nhds_within_of_tendsto_nhds;
  assumption
end

theorem has_deriv_at.lhopital_zero_at_top
  (hff' : ∀ᶠ x in at_top, has_deriv_at f (f' x) x)
  (hgg' : ∀ᶠ x in at_top, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in at_top, g' x ≠ 0)
  (hftop : tendsto f at_top (𝓝 0)) (hgtop : tendsto g at_top (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) at_top l) :
  tendsto (λ x, (f x) / (g x)) at_top l :=
begin
  rw eventually_iff_exists_mem at *,
  rcases hff' with ⟨s₁, hs₁, hff'⟩,
  rcases hgg' with ⟨s₂, hs₂, hgg'⟩,
  rcases hg' with ⟨s₃, hs₃, hg'⟩,
  let s := s₁ ∩ s₂ ∩ s₃,
  have hs : s ∈ at_top := inter_mem_sets (inter_mem_sets hs₁ hs₂) hs₃,
  rw mem_at_top_sets at hs,
  rcases hs with ⟨l, hl⟩,
  have hl' : Ioi l ⊆ s := λ x hx, hl x (le_of_lt hx),
  refine has_deriv_at.lhopital_zero_at_top_on_Ioi _ _ (λ x hx, hg' x $ (hl' hx).2) hftop hgtop hdiv;
  intros x hx;
  apply_assumption;
  exact (hl' hx).1.1 <|> exact (hl' hx).1.2
end

theorem deriv.lhopital_zero_at_top
  (hdf : ∀ᶠ (x : ℝ) in at_top, differentiable_at ℝ f x)
  (hg' : ∀ᶠ (x : ℝ) in at_top, deriv g x ≠ 0)
  (hftop : tendsto f at_top (𝓝 0)) (hgtop : tendsto g at_top (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) at_top l) :
  tendsto (λ x, (f x) / (g x)) at_top l :=
begin
  have hdg : ∀ᶠ x in at_top, differentiable_at ℝ g x,
    from hg'.mp (eventually_of_forall $ 
      λ _ hg', decidable.by_contradiction (λ h, hg' (deriv_zero_of_not_differentiable_at h))),
  have hdf' : ∀ᶠ x in at_top, has_deriv_at f (deriv f x) x,
    from hdf.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  have hdg' : ∀ᶠ x in at_top, has_deriv_at g (deriv g x) x,
    from hdg.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  exact has_deriv_at.lhopital_zero_at_top hdf' hdg' hg' hftop hgtop hdiv
end

theorem has_deriv_at.lhopital_zero_at_bot
  (hff' : ∀ᶠ x in at_bot, has_deriv_at f (f' x) x)
  (hgg' : ∀ᶠ x in at_bot, has_deriv_at g (g' x) x)
  (hg' : ∀ᶠ x in at_bot, g' x ≠ 0)
  (hfbot : tendsto f at_bot (𝓝 0)) (hgbot : tendsto g at_bot (𝓝 0))
  (hdiv : tendsto (λ x, (f' x) / (g' x)) at_bot l) :
  tendsto (λ x, (f x) / (g x)) at_bot l :=
begin
  rw eventually_iff_exists_mem at *,
  rcases hff' with ⟨s₁, hs₁, hff'⟩,
  rcases hgg' with ⟨s₂, hs₂, hgg'⟩,
  rcases hg' with ⟨s₃, hs₃, hg'⟩,
  let s := s₁ ∩ s₂ ∩ s₃,
  have hs : s ∈ at_bot := inter_mem_sets (inter_mem_sets hs₁ hs₂) hs₃,
  rw mem_at_bot_sets at hs,
  rcases hs with ⟨l, hl⟩,
  have hl' : Iio l ⊆ s := λ x hx, hl x (le_of_lt hx),
  refine has_deriv_at.lhopital_zero_at_bot_on_Iio _ _ (λ x hx, hg' x $ (hl' hx).2) hfbot hgbot hdiv;
  intros x hx;
  apply_assumption;
  exact (hl' hx).1.1 <|> exact (hl' hx).1.2
end

theorem deriv.lhopital_zero_at_bot
  (hdf : ∀ᶠ (x : ℝ) in at_bot, differentiable_at ℝ f x)
  (hg' : ∀ᶠ (x : ℝ) in at_bot, deriv g x ≠ 0)
  (hfbot : tendsto f at_bot (𝓝 0)) (hgbot : tendsto g at_bot (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) at_bot l) :
  tendsto (λ x, (f x) / (g x)) at_bot l :=
begin
  have hdg : ∀ᶠ x in at_bot, differentiable_at ℝ g x,
    from hg'.mp (eventually_of_forall $ 
      λ _ hg', decidable.by_contradiction (λ h, hg' (deriv_zero_of_not_differentiable_at h))),
  have hdf' : ∀ᶠ x in at_bot, has_deriv_at f (deriv f x) x,
    from hdf.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  have hdg' : ∀ᶠ x in at_bot, has_deriv_at g (deriv g x) x,
    from hdg.mp (eventually_of_forall $ λ _, differentiable_at.has_deriv_at),
  exact has_deriv_at.lhopital_zero_at_bot hdf' hdg' hg' hfbot hgbot hdiv
end

#lint

#check Ioi_mem_nhds

/-
example : tendsto (λ x, real.sin x / real.log (1+x)) (𝓝[univ \ {0}] 0) (𝓝 1) :=
begin
  apply lhopital_zero_nhds,
  { exact eventually_of_forall (λ x, real.differentiable_at_sin) },
  { rw eventually_iff_exists_mem,
    use [Ioi (-1), Ioi_mem_nhds $ by norm_num],
    intros x hx,
    have : 0 < 1 + x,
    { rw mem_Ioi at hx, linarith },
    suffices h : deriv (λ (x : ℝ), (1 + x).log) x = (1+x)⁻¹,
    { rw h,
      apply inv_ne_zero,
      apply ne_of_gt,
      linarith [hx] },
    rw deriv_log',
    { simp },
    { simp },
    { linarith } },
  { convert real.continuous_sin.tendsto 0,
    exact real.sin_zero.symm },
  { convert continuous_at.tendsto _,
    { simp },
    { apply (real.continuous_at_log _).comp,
      { apply continuous.continuous_at,
        continuity },
      { norm_num } } },
  { simp [deriv_log'], }
end
-/

/-
-- TODO : Is this even useful ?
theorem lhopital_zero_inside_Ioo {a b c : ℝ} (hab : a < b) (hbc : b < c) (f g : ℝ → ℝ) {l : filter ℝ}
  (hdf : differentiable_on ℝ f s)
  (hg' : ∀ x ∈ Ioo a c, x ≠ b → deriv g x ≠ 0)
  (hfb : f b = 0) (hgb : g b = 0)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b (Iio b ∪ Ioi b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b ∪ Ioi b)) l :=
begin
  rw nhds_within_union at *,
  rw tendsto_sup at *,
  have sub1 : Ioo a b ⊆ Ioo a c := Ioo_subset_Ioo (le_refl _) (le_of_lt hbc),
  have sub2 : Ioo b c ⊆ Ioo a c := Ioo_subset_Ioo (le_of_lt hab) (le_refl _),
  exact ⟨ @lhopital_zero_at_right_of_Ioc a b hab f g l
            (differentiable_on.mono hdf sub1)
            (differentiable_on.mono hdg sub1)
            (continuous_on.mono (differentiable_on.continuous_on hdf) (Ioc_subset_Ioo_right hbc))
            (continuous_on.mono (differentiable_on.continuous_on hdg) (Ioc_subset_Ioo_right hbc))
            (λ x hx, hg' x (sub1 hx) $ λ h, by{rw h at hx, exact right_mem_Ioo.mp hx})
            hfb
            hgb
            hdiv.1,
          @lhopital_zero_at_left_of_Ico b c hbc f g l
            (differentiable_on.mono hdf sub2)
            (differentiable_on.mono hdg sub2)
            (continuous_on.mono (differentiable_on.continuous_on hdf) (Ico_subset_Ioo_left hab))
            (continuous_on.mono (differentiable_on.continuous_on hdg) (Ico_subset_Ioo_left hab))
            (λ x hx, hg' x (sub2 hx) $ λ h, by{rw h at hx, exact left_mem_Ioo.mp hx})
            hfb
            hgb
            hdiv.2 ⟩
end
-/

-- TODO : do I want to reformulate to :
/-
theorem lhopital_zero_of_mem_nhds {a : ℝ} {s : set ℝ} (s ∈ 𝓝 a) {f g : ℝ → ℝ} {l : filter ℝ}
  (hdg : differentiable_on ℝ g $ s \ {a})
  (hg' : ∀ x ∈ s, x ≠ a → deriv g x ≠ 0)
  (hfb : f b = 0) (hgb : g b = 0)
  (hdivl : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b $ Iio b) l)
  (hdivr : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b $ Ioi b) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b ∪ Ioi b)) l := sorry
-/

/-
theorem lhopital_zero_at_left_of_Ioo_right_of_Ioo {a b c : ℝ} {hab : a < b} {hbc : b < c} {f g : ℝ → ℝ} {l : filter ℝ}
  (hdf : differentiable_on ℝ f (Ioo a b ∪ Ioo b c)) (hdg : differentiable_on ℝ g (Ioo a b ∪ Ioo b c))
  (hg' : ∀ x ∈ (Ioo a b ∪ Ioo b c), x ≠ b → deriv g x ≠ 0)
  (hfb : tendsto f (nhds_within b (Iio b ∪ Ioi b)) (𝓝 0)) (hgb : tendsto g (nhds_within b (Iio b ∪ Ioi b)) (𝓝 0))
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b (Iio b ∪ Ioi b)) l) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b ∪ Ioi b)) l :=
begin
  rw nhds_within_union at *,
  rw tendsto_sup at *,
  have sub1 := subset_union_left (Ioo a b) (Ioo b c),
  have sub2 := subset_union_right (Ioo a b) (Ioo b c),
  exact ⟨ @lhopital_zero_at_right_of_Ioo a b hab f g l
            (hdf.mono sub1)
            (hdg.mono sub1)
            (λ x hx, hg' x (sub1 hx) $ λ h, by{rw h at hx, exact right_mem_Ioo.mp hx})
            hfb.1
            hgb.1
            hdiv.1,
          @lhopital_zero_at_left_of_Ioo b c hbc f g l
            (hdf.mono sub2)
            (hdg.mono sub2)
            (λ x hx, hg' x (sub2 hx) $ λ h, by{rw h at hx, exact left_mem_Ioo.mp hx})
            hfb.2
            hgb.2
            hdiv.2 ⟩
end
-/

--

theorem lhopital_at_top_at_left_of_Ioo_tendsto_nhds {a b l : ℝ} {hab : a < b} (f g : ℝ → ℝ)
  (hdf : differentiable_on ℝ f (Ioo a b)) (hdg : differentiable_on ℝ g (Ioo a b))
  (hg' : ∀ x ∈ Ioo a b, deriv g x ≠ 0)
  (hga : tendsto (λ x, ∥ g x ∥) (nhds_within a (Ioi a)) at_top)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within a (Ioi a)) (𝓝 l)) :
  tendsto (λ x, (f x) / (g x)) (nhds_within a (Ioi a)) (𝓝 l) :=
begin
  have := eventually_ne_of_tendsto_norm_at_top hga 0,
    rw [eventually_iff, mem_nhds_within_Ioi_iff_exists_Ioo_subset] at this,
    rcases this with ⟨ u, hu, this ⟩,
  set r := min u (a + (b-a)/2),
  have har : a < r := lt_min hu (lt_add_of_pos_right a (by linarith)),
  have sub0 : Ioc a r ⊆ Ioo a b,
    from λ x hx, ⟨ hx.1, lt_of_le_of_lt hx.2 $ min_lt_iff.mpr $ or.inr $ add_midpoint hab ⟩,
  have sub1 : ∀ y ∈ Ioc a r, ∀ x ∈ Ioo a y, Icc x y ⊆ Ioc a r,
    from λ y hy x hx z hz, ⟨ lt_of_lt_of_le hx.1 hz.1, le_trans hz.2 hy.2⟩,
  have hg : ∀ x ∈ Ioo a r, g x ≠ 0,
    from λ x hx, this $ Ioo_subset_Ioo (le_refl _) (min_le_left _ _) hx,
  have factA : ∀ y ∈ Ioc a r, ∀ x ∈ Ioo a y, ∃ c ∈ Ioo x y, 
    f x / g x = (deriv f c / deriv g c) - (g y) / (g x) * (deriv f c / deriv g c) + (f y / g x),
  { intros y hy x hx,
    have := exists_ratio_deriv_eq_ratio_slope g hx.2
            ( hdg.continuous_on.mono (trans (sub1 y hy x hx) sub0) )
            ( hdg.mono (trans (trans Ioo_subset_Icc_self (sub1 y hy x hx)) sub0) ) f
            ( hdf.continuous_on.mono (trans (sub1 y hy x hx) sub0) )
            ( hdf.mono (trans (trans Ioo_subset_Icc_self (sub1 y hy x hx)) sub0) ),
    rcases this with ⟨ c, hc, this ⟩,
    use c,
    use hc,
    rw [← mul_right_inj', mul_add, mul_sub],
    repeat {rw mul_div_cancel'},
    rw [← sub_eq_iff_eq_add, ← mul_assoc, mul_div_cancel', ← sub_mul,
        ← mul_div_assoc, ← mul_left_inj', div_mul_cancel, ← neg_sub,
        ← neg_sub (g y) (g x)],
    ring,
    rwa this,
    any_goals {exact hg x ⟨ hx.1, lt_of_lt_of_le hx.2 hy.2 ⟩},
    all_goals {exact hg' c (sub0 $ sub1 y hy x hx $ Ioo_subset_Icc_self hc)} },
  rw metric.tendsto_nhds_within_nhds at *,
  intros ε hε,
  specialize hdiv (ε/3) (by linarith),
  rcases hdiv with ⟨ δ₁, hδ₁pos, hδ₁ ⟩,
  set y := min r (a + δ₁), 
  rw gt_iff_lt at hδ₁pos,
  have hy : y ∈ Ioc a r,
    from⟨ lt_min har (lt_add_of_pos_right _ hδ₁pos), 
          min_le_left _ _⟩,
  have hyε : ∀ t ∈ Ioo a y, dist (deriv f t / deriv g t) l < ε/3,
  { intros t ht, 
    apply hδ₁ ht.1,
    rw [real.dist_eq, abs_lt],
    exact ⟨ lt_trans (neg_lt_zero.mpr hδ₁pos) (sub_pos.mpr ht.left),
            sub_lt_iff_lt_add'.mpr 
            ( lt_of_lt_of_le ht.right (min_le_right r (a + δ₁)) ) ⟩ },
  have : ∀ t, tendsto (λ x, t / g x) (nhds_within a (Ioi a)) (𝓝 0),
  { intros t,
    rw tendsto_zero_iff_norm_tendsto_zero,
    simp only [normed_field.norm_div],
    ring,
    rw ← (show 0 * ∥ t ∥ = 0, from zero_mul _),
    exact (tendsto_inv_at_top_zero.comp hga).mul tendsto_const_nhds },
  simp_rw [metric.tendsto_nhds_within_nhds, dist_zero_right] at this,
  rcases this (g y) ((ε/3)/(∥l∥ + ε/3))
              ( div_pos (by linarith) (by linarith [norm_nonneg l]) )
    with ⟨ η₁, hη₁, hη₁ε ⟩,
  rcases this (f y) (ε/3) (by linarith) with ⟨ η₂, hη₂, hη₂ε ⟩,
  set δ := min ((y-a)/2) (min η₁ η₂),
  set x₀ := a + δ,
  use δ,
  use (lt_min (by linarith [hy.1]) (lt_min (by linarith) (by linarith))),
  intros x hxa hxδ, 
  have hxx₀ : x < x₀,
  { rw [real.dist_eq, abs_lt, sub_lt_iff_lt_add'] at hxδ,
    exact hxδ.2 },
  have hxay : x ∈ Ioo a y := 
  ⟨ hxa, lt_trans hxx₀ (add_lt_of_lt_sub_left (min_lt_iff.mpr $ or.inl (by linarith [hy.1]))) ⟩,
  specialize  factA y hy x hxay,
  rcases factA with ⟨ c, hc, h ⟩,
  specialize hη₁ε hxa 
    ( lt_of_lt_of_le hxδ 
      ( le_trans 
        ( min_le_right ((y-a)/2) (min η₁ η₂) )
        ( min_le_left η₁ η₂ ) ) ),
  specialize hη₂ε hxa 
    ( lt_of_lt_of_le hxδ 
      ( le_trans 
        ( min_le_right ((y-a)/2) (min η₁ η₂) )
        ( min_le_right η₁ η₂ ) ) ),
  specialize hyε c ⟨ lt_trans hxa hc.1, hc.2 ⟩,
  rw dist_eq_norm at *,
  have hyε' : ∥ deriv f c / deriv g c ∥ < ∥l∥ + ε/3,
  { rw (show deriv f c / deriv g c = _ - l + l, by simp),
    apply lt_of_le_of_lt (norm_add_le _ _),
    linarith },
  rw h,
  calc ∥deriv f c / deriv g c - g y / g x * (deriv f c / deriv g c) + f y / g x - l∥ = _ : rfl
    ... = ∥(deriv f c / deriv g c - l) + (-g y / g x * (deriv f c / deriv g c) + f y / g x)∥ : 
        by { congr, ring }
    ... ≤ ∥(deriv f c / deriv g c - l)∥ + ∥(-g y / g x * (deriv f c / deriv g c)) + f y / g x∥ : 
        norm_add_le _ _
    ... ≤ ∥(deriv f c / deriv g c - l)∥ + (∥(-g y / g x * (deriv f c / deriv g c))∥ + ∥f y / g x∥) : 
        add_le_add_left (norm_add_le _ _) _
    ... = ∥deriv f c / deriv g c - l∥ + ∥g y / g x∥ * ∥deriv f c / deriv g c∥ + ∥f y / g x∥ : 
        by { simp only [normed_field.norm_mul, norm_neg, normed_field.norm_div], abel }
    ... < (ε/3) + (ε/3) + (ε/3) : 
        by{ apply add_lt_add,
            apply add_lt_add,
            exact hyε,
            rw( show ε/3 = (ε/3 / (∥l∥ + ε/3)) * (∥l∥ + ε/3), 
                by{ rw div_mul_cancel,
                    linarith [norm_nonneg l] }), 
            exact mul_lt_mul'' hη₁ε hyε' (norm_nonneg _) (norm_nonneg _),
            exact hη₂ε }
    ... = ε : by ring
end

theorem lhopital_at_top_at_right_of_Ioo_tendsto_nhds {a b l : ℝ} {hab : a < b} (f g : ℝ → ℝ)
  (hdf : differentiable_on ℝ f (Ioo a b)) (hdg : differentiable_on ℝ g (Ioo a b))
  (hg' : ∀ x ∈ Ioo a b, deriv g x ≠ 0)
  (hgb : tendsto (λ x, ∥ g x ∥) (nhds_within b (Iio b)) at_top)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within b (Iio b)) (𝓝 l)) :
  tendsto (λ x, (f x) / (g x)) (nhds_within b (Iio b)) (𝓝 l) :=
begin
  have hdnf : differentiable_on ℝ (f ∘ has_neg.neg) (Ioo (-b) (-a)) :=
    (differentiable_on.comp hdf (differentiable_on_neg _) (show Ioo (-b) (-a) ⊆ preimage has_neg.neg (Ioo a b), by rw [preimage_neg, image_neg, preimage_neg_Ioo])),
  have hdnf' : ∀ x ∈ -Ioo a b, differentiable_at ℝ f (-x) :=
    λ x hx, differentiable_within_at.differentiable_at (hdf (-x) hx) (Ioo_mem_nhds hx.1 hx.2),
  have hdng : differentiable_on ℝ (g ∘ has_neg.neg) (Ioo (-b) (-a)) :=
    (differentiable_on.comp hdg (differentiable_on_neg _) (show Ioo (-b) (-a) ⊆ preimage has_neg.neg (Ioo a b), by rw [preimage_neg, image_neg, preimage_neg_Ioo])),
  have hdng' : ∀ x ∈ -Ioo a b, differentiable_at ℝ g (-x) :=
    λ x hx, differentiable_within_at.differentiable_at (hdg (-x) hx) (Ioo_mem_nhds hx.1 hx.2),
  have := lhopital_at_top_at_left_of_Ioo_tendsto_nhds (f ∘ has_neg.neg) (g ∘ has_neg.neg) hdnf hdng
    (by { intros x hx h,
          apply hg', 
          rw ← preimage_neg_Ioo at hx, 
          exact hx, 
          rw ← neg_eq_zero, 
          rwa [deriv.comp, deriv_neg, mul_comm, ← neg_eq_neg_one_mul] at h,
          rwa ← preimage_neg_Ioo at hx,
          exact hdng' x hx,
          exact differentiable_neg _ })
    (hgb.comp tendsto_neg_nhds_within_Ioi_neg)
    (by { rw ← nhds_within_Ioo_eq_nhds_within_Ioi,
          have : ∀ x ∈ Ioo (-b) (-a), ((λ (x : ℝ), deriv f x / deriv g x) ∘ has_neg.neg) x = deriv (f ∘ has_neg.neg) x / deriv (g ∘ has_neg.neg) x, 
          { intros x hx,
            rw ← preimage_neg_Ioo at hx,
            symmetry,
            rw deriv.comp, 
            rw @deriv.comp _ _ _ g, 
            simp only [neg_div_neg_eq, deriv_neg', mul_one, deriv_id'', mul_neg_eq_neg_mul_symm],
            any_goals { exact differentiable_neg _ },
            exact hdng' _ hx,
            exact hdnf' _ hx },
          apply tendsto_nhds_within_congr this,
          simp only,
          apply hdiv.comp, 
          rw nhds_within_Ioo_eq_nhds_within_Ioi,
          exact tendsto_neg_nhds_within_Ioi_neg,
          all_goals { exact neg_lt_neg hab } }),
  have := this.comp tendsto_neg_nhds_within_Iio,
  unfold function.comp at this,
  simp only [neg_neg] at this,
  exact this,
  exact neg_lt_neg hab,
end

theorem lhopital_at_top_at_top_tendsto_nhds {a l : ℝ} (f g : ℝ → ℝ)
  (hdf : differentiable_on ℝ f (Ioi a)) (hdg : differentiable_on ℝ g (Ioi a))
  (hg' : ∀ x ∈ Ioi a, deriv g x ≠ 0)
  (hgb : tendsto (λ x, ∥ g x ∥) at_top at_top)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) at_top (𝓝 l)) :
  tendsto (λ x, (f x) / (g x)) at_top (𝓝 l) :=
begin
  have : ∃ a', a < a' ∧ 0 < a' := by { use 1 + max a 0, exact ⟨lt_of_le_of_lt (le_max_left a 0) (lt_one_add _), lt_of_le_of_lt (le_max_right a 0) (lt_one_add _)⟩ },
  rcases this with ⟨ a', haa', ha'⟩,
  have fact1 : ∀ x ∈ Ioo 0 a'⁻¹, (x : ℝ) ≠ 0, { intros x hx, rw mem_Ioo at hx, symmetry, exact ne_of_lt hx.1 },
  have fact2 : ∀ x ∈ Ioo 0 a'⁻¹, a < x⁻¹, { intros x hx, rw mem_Ioo at hx, apply lt_trans haa', rw lt_inv, exact hx.2, exact ha', exact hx.1 },
  have := lhopital_at_top_at_left_of_Ioo_tendsto_nhds (f ∘ has_inv.inv) (g ∘ has_inv.inv)
    (show differentiable_on ℝ (f ∘ has_inv.inv) (Ioo 0 (a'⁻¹)), 
      by {apply differentiable_on.comp, exact hdf, apply differentiable_on.mono differentiable_on_inv, rintros x ⟨hx, _⟩, simp only [mem_set_of_eq], symmetry, exact ne_of_lt hx,
          rintros x hx, rw mem_preimage, rw mem_Ioi, exact fact2 x hx})
    (by {apply differentiable_on.comp, exact hdg, apply differentiable_on.mono differentiable_on_inv, rintros x ⟨hx, _⟩, simp only [mem_set_of_eq], symmetry, exact ne_of_lt hx,
        rintros x hx, rw mem_preimage, rw mem_Ioi, exact fact2 x hx})
    (by { intros x hx,
          rw [deriv.comp, deriv_inv],
          apply mul_ne_zero,
          intros h, 
          apply hg', 
          any_goals {exact h},
          any_goals {rw neg_ne_zero, apply inv_ne_zero, apply pow_ne_zero},
          any_goals {apply (hdg x⁻¹ _).differentiable_at},
          any_goals {apply Ioi_mem_nhds},
          any_goals {apply differentiable_at_inv},
          all_goals { try{rw mem_Ioi}, apply_assumption, exact hx } })
    (hgb.comp tendsto_inv_zero_at_top)
    (by { rw ← nhds_within_Ioo_eq_nhds_within_Ioi,
          have : ∀ x ∈ Ioo 0 a'⁻¹, ((λ x, deriv f x / deriv g x) ∘ has_inv.inv) x = deriv (f ∘ has_inv.inv) x / deriv (g ∘ has_inv.inv) x,
          { intros x hx,
            symmetry,
            rw deriv.comp, 
            rw @deriv.comp _ _ _ g,
            rw mul_div_mul_right,
            rw [deriv_inv, neg_ne_zero],
            apply inv_ne_zero,
            apply pow_ne_zero, 
            any_goals {apply differentiable_at_inv},
            any_goals {apply differentiable_within_at.differentiable_at},
            any_goals {apply hdf},
            any_goals {apply hdg},
            any_goals {apply Ioi_mem_nhds},
            any_goals {rw mem_Ioi},
            all_goals {apply_assumption, exact hx} },
          apply tendsto_nhds_within_congr this,
          simp only,
          rw nhds_within_Ioo_eq_nhds_within_Ioi,
          exact tendsto.comp hdiv tendsto_inv_zero_at_top,
          repeat {rwa inv_pos} }),
  have := tendsto.comp this tendsto_inv_at_top_zero',
  unfold function.comp at this,
  simp only [inv_inv'] at this,
  exact this,
  rwa inv_pos,
end

theorem lhopital_at_top_at_left_of_Ioo_tendsto_at_top {a b: ℝ} {hab : a < b} (f g : ℝ → ℝ)
  (hdf : differentiable_on ℝ f (Ioo a b)) (hdg : differentiable_on ℝ g (Ioo a b))
  (hg' : ∀ x ∈ Ioo a b, deriv g x ≠ 0)
  (hga : tendsto (λ x, ∥ g x ∥) (nhds_within a (Ioi a)) at_top)
  (hdiv : tendsto (λ x, ((deriv f) x) / ((deriv g) x)) (nhds_within a (Ioi a)) at_top) :
  tendsto (λ x, (f x) / (g x)) (nhds_within a (Ioi a)) at_top :=
begin
  have := eventually_ne_of_tendsto_norm_at_top hga 0,
    rw [eventually_iff, mem_nhds_within_Ioi_iff_exists_Ioo_subset] at this,
    rcases this with ⟨ u, hu, this ⟩,
  set r := min u (a + (b-a)/2),
  have har : a < r := lt_min hu (lt_add_of_pos_right a (by linarith)),
  have sub0 : Ioc a r ⊆ Ioo a b,
    from λ x hx, ⟨ hx.1, lt_of_le_of_lt hx.2 $ min_lt_iff.mpr $ or.inr $ add_midpoint hab ⟩,
  have sub1 : ∀ y ∈ Ioc a r, ∀ x ∈ Ioo a y, Icc x y ⊆ Ioc a r,
    from λ y hy x hx z hz, ⟨ lt_of_lt_of_le hx.1 hz.1, le_trans hz.2 hy.2⟩,
  have hg : ∀ x ∈ Ioo a r, g x ≠ 0,
    from λ x hx, this $ Ioo_subset_Ioo (le_refl _) (min_le_left _ _) hx,
  have factA : ∀ y ∈ Ioc a r, ∀ x ∈ Ioo a y, ∃ c ∈ Ioo x y, 
    f x / g x = (deriv f c / deriv g c) - (g y) / (g x) * (deriv f c / deriv g c) + (f y / g x),
  { intros y hy x hx,
    have := exists_ratio_deriv_eq_ratio_slope g hx.2
            ( hdg.continuous_on.mono (trans (sub1 y hy x hx) sub0) )
            ( hdg.mono (trans (trans Ioo_subset_Icc_self (sub1 y hy x hx)) sub0) ) f
            ( hdf.continuous_on.mono (trans (sub1 y hy x hx) sub0) )
            ( hdf.mono (trans (trans Ioo_subset_Icc_self (sub1 y hy x hx)) sub0) ),
    rcases this with ⟨ c, hc, this ⟩,
    use c,
    use hc,
    rw [← mul_right_inj', mul_add, mul_sub],
    repeat {rw mul_div_cancel'},
    rw [← sub_eq_iff_eq_add, ← mul_assoc, mul_div_cancel', ← sub_mul,
        ← mul_div_assoc, ← mul_left_inj', div_mul_cancel, ← neg_sub,
        ← neg_sub (g y) (g x)],
    ring,
    rwa this,
    any_goals {exact hg x ⟨ hx.1, lt_of_lt_of_le hx.2 hy.2 ⟩},
    all_goals {exact hg' c (sub0 $ sub1 y hy x hx $ Ioo_subset_Icc_self hc)} },
  rw metric.tendsto_nhds_within_at_top at ⊢ hdiv,
  intros M,
  specialize hdiv (∥M∥+2),
  rcases hdiv with ⟨ δ₁, hδ₁pos, hδ₁ ⟩,
  set y := min r (a + δ₁), 
  rw gt_iff_lt at hδ₁pos,
  have hy : y ∈ Ioc a r,
    from⟨ lt_min har (lt_add_of_pos_right _ hδ₁pos), 
          min_le_left _ _⟩,
  /-have hyε : ∀ t ∈ Ioo a y, dist (deriv f t / deriv g t) l < ε/3,
  { intros t ht, 
    apply hδ₁ ht.1,
    rw [real.dist_eq, abs_lt],
    exact ⟨ lt_trans (neg_lt_zero.mpr hδ₁pos) (sub_pos.mpr ht.left),
            sub_lt_iff_lt_add'.mpr 
            ( lt_of_lt_of_le ht.right (min_le_right r (a + δ₁)) ) ⟩ },-/
  have : ∀ t, tendsto (λ x, t / g x) (nhds_within a (Ioi a)) (𝓝 0),
  { intros t,
    rw tendsto_zero_iff_norm_tendsto_zero,
    simp only [normed_field.norm_div],
    ring,
    rw ← (show 0 * ∥ t ∥ = 0, from zero_mul _),
    exact (tendsto_inv_at_top_zero.comp hga).mul tendsto_const_nhds },
  simp_rw [metric.tendsto_nhds_within_nhds, dist_zero_right] at this,
  rcases this (g y) (1/(∥M∥+2))
              ( div_pos (by linarith) (by linarith [norm_nonneg M]) )
    with ⟨ η₁, hη₁, hη₁ε ⟩,
  rcases this (f y) 1 (by norm_num) with ⟨ η₂, hη₂, hη₂ε ⟩,
  set δ := min ((y-a)/2) (min η₁ η₂),
  set x₀ := a + δ,
  use δ,
  use (lt_min (by linarith [hy.1]) (lt_min (by linarith) (by linarith))),
  intros x hxa hxδ, 
  have hxx₀ : x < x₀,
  { rw [real.dist_eq, abs_lt, sub_lt_iff_lt_add'] at hxδ,
    exact hxδ.2 },
  have hxay : x ∈ Ioo a y := 
  ⟨ hxa, lt_trans hxx₀ (add_lt_of_lt_sub_left (min_lt_iff.mpr $ or.inl (by linarith [hy.1]))) ⟩,
  specialize  factA y hy x hxay,
  rcases factA with ⟨ c, hc, h ⟩,
  specialize hη₁ε hxa 
    ( lt_of_lt_of_le hxδ 
      ( le_trans 
        ( min_le_right ((y-a)/2) (min η₁ η₂) )
        ( min_le_left η₁ η₂ ) ) ),
  specialize hη₂ε hxa 
    ( lt_of_lt_of_le hxδ 
      ( le_trans 
        ( min_le_right ((y-a)/2) (min η₁ η₂) )
        ( min_le_right η₁ η₂ ) ) ),
  --specialize hyε c ⟨ lt_trans hxa hc.1, hc.2 ⟩,
  /-rw dist_eq_norm at *,
  have hyε' : ∥ deriv f c / deriv g c ∥ < ∥l∥ + ε/3,
  { rw (show deriv f c / deriv g c = _ - l + l, by simp),
    apply lt_of_le_of_lt (norm_add_le _ _),
    linarith },-/
  rw h,
  calc ∥deriv f c / deriv g c - g y / g x * (deriv f c / deriv g c) + f y / g x - l∥ = _ : rfl
    ... = ∥(deriv f c / deriv g c - l) + (-g y / g x * (deriv f c / deriv g c) + f y / g x)∥ : 
        by { congr, ring }
    ... ≤ ∥(deriv f c / deriv g c - l)∥ + ∥(-g y / g x * (deriv f c / deriv g c)) + f y / g x∥ : 
        norm_add_le _ _
    ... ≤ ∥(deriv f c / deriv g c - l)∥ + (∥(-g y / g x * (deriv f c / deriv g c))∥ + ∥f y / g x∥) : 
        add_le_add_left (norm_add_le _ _) _
    ... = ∥deriv f c / deriv g c - l∥ + ∥g y / g x∥ * ∥deriv f c / deriv g c∥ + ∥f y / g x∥ : 
        by { simp only [normed_field.norm_mul, norm_neg, normed_field.norm_div], abel }
    ... < (ε/3) + (ε/3) + (ε/3) : 
        by{ apply add_lt_add,
            apply add_lt_add,
            exact hyε,
            rw( show ε/3 = (ε/3 / (∥l∥ + ε/3)) * (∥l∥ + ε/3), 
                by{ rw div_mul_cancel,
                    linarith [norm_nonneg l] }), 
            exact mul_lt_mul'' hη₁ε hyε' (norm_nonneg _) (norm_nonneg _),
            exact hη₂ε }
    ... = ε : by ring
end