import topology.continuous_on
import tactic.tidy
import analysis.calculus.deriv
import analysis.calculus.mean_value
import analysis.calculus.local_extr
import topology.instances.real
import tactic.tfae
import extend

open filter asymptotics continuous_linear_map set
open_locale filter topological_space classical

noncomputable theory

attribute [instance] classical.prop_decidable

#check eventually_iff

#check mem_nhds_within_of_mem_nhds

#check eventually_nhds_with_of_forall

lemma eventually_nhds_within_of_eventually_nhds {α : Type*} [topological_space α]
  {s : set α} {a : α} {p : α → Prop} (h : ∀ᶠ x in 𝓝 a, p x) :
  ∀ᶠ x in 𝓝[s] a, p x :=
mem_nhds_within_of_mem_nhds h

lemma at_bot_basis {α : Type*} [nonempty α] [semilattice_inf α] :
  (@at_bot α _).has_basis (λ _, true) Iic :=
has_basis_infi_principal (directed_of_inf $ λ a b, Iic_subset_Iic.2)

@[simp, nolint ge_or_gt]
lemma mem_at_bot_sets {α : Type*} [nonempty α] [semilattice_inf α] {s : set α} :
  s ∈ (at_bot : filter α) ↔ ∃a:α, ∀b≤a, b ∈ s :=
at_bot_basis.mem_iff.trans $ exists_congr $ λ _, exists_const _

namespace metric

#check tendsto_nhds

#check nhds_within_basis_ball

#check has_basis.eventually_iff

lemma eventually_nhds {α : Type*} [metric_space α] {a : α} {p : α → Prop} :
  (∀ᶠ x in (𝓝 a), p x) ↔ ∃ δ > 0, ∀{x:α}, dist x a < δ → p x :=
by simp only [nhds_basis_ball.eventually_iff, exists_prop, mem_ball]

lemma eventually_nhds_within {α : Type*} [metric_space α] {a : α} {s : set α} {p : α → Prop} :
  (∀ᶠ x in (nhds_within a s), p x) ↔ ∃ δ > 0, ∀{x:α}, x ∈ s → dist x a < δ → p x :=
by simp only [nhds_within_basis_ball.eventually_iff, imp.swap, and_imp, exists_prop, mem_ball, mem_inter_eq]

/-
--Useless ?
lemma tendsto_nhds_any {α β : Type*} [metric_space α] {f : filter β} {g : α → β} {a : α} :
  tendsto g (𝓝 a) f ↔
    ∀ s ∈ f, ∃ δ > 0, ∀{x:α}, dist x a < δ → g x ∈ s :=
nhds_basis_ball.tendsto_left_iff

--Useless ?
lemma tendsto_nhds_within_any {α β : Type*} [metric_space α] {f : filter β} {g : α → β} {a : α} {s : set α} :
  tendsto g (nhds_within a s) f ↔
    ∀ u ∈ f, ∃ δ > 0, ∀{x:α}, x ∈ s → dist x a < δ → g x ∈ u :=
by simp only [nhds_within_basis_ball.tendsto_left_iff, imp.swap, and_imp, mem_ball, mem_inter_eq]
-/

/- 
Already exists
lemma tendsto_nhds {α β : Type*} [metric_space β] {f : filter α} {g : α → β} {b : β} :
  tendsto g f (𝓝 b) ↔
    ∀ ε > 0, (∀ᶠ x in f, dist (g x) b < ε) :=
nhds_basis_ball.tendsto_right_iff
-/

lemma tendsto_nhds_within {α β : Type*} [metric_space β] {f : filter α} {g : α → β} {b : β} {s : set β} :
  tendsto g f (nhds_within b s) ↔
    ∀ ε > 0, (∀ᶠ x in f, dist (g x) b < ε ∧ g x ∈ s) :=
by simp only [nhds_within_basis_ball.tendsto_right_iff, mem_ball, mem_inter_eq]

end metric

--

theorem metric.tendsto_nhds_at_top {α β : Type*} [metric_space α] [preorder β] {f : α → β} {a : α} :
  tendsto f (𝓝 a) at_top ↔
    ∀ M : β, ∃ δ > 0, ∀{x:α}, dist x a < δ → M ≤ f x :=
by simp [tendsto_at_top, metric.eventually_nhds]

theorem metric.tendsto_nhds_at_bot {α β : Type*} [metric_space α] [preorder β] {f : α → β} {a : α} :
  tendsto f (𝓝 a) at_bot ↔
    ∀ M : β, ∃ δ > 0, ∀{x:α}, dist x a < δ → f x ≤ M :=
@metric.tendsto_nhds_at_top _ (order_dual β) _ _ _ _

theorem metric.tendsto_nhds_principal {α β : Type*} [metric_space α] {f : α → β} {a : α} {s : set β} :
  tendsto f (𝓝 a) (𝓟 s) ↔ ∃ δ > 0, ∀{x:α}, dist x a < δ → f x ∈ s :=
by rw [tendsto_principal, metric.eventually_nhds]

theorem metric.tendsto_nhds_within_at_top {α β : Type*} [metric_space α] [preorder β] {f : α → β} {a : α} {s : set α} :
  tendsto f (nhds_within a s) at_top ↔
    ∀ M : β, ∃ δ > 0, ∀{x:α}, x ∈ s → dist x a < δ → M ≤ f x :=
by simp [tendsto_at_top, metric.eventually_nhds_within]

theorem metric.tendsto_nhds_within_at_bot {α β : Type*} [metric_space α] [preorder β] {f : α → β} {a : α} {s : set α} :
  tendsto f (nhds_within a s) at_bot ↔
    ∀ M : β, ∃ δ > 0, ∀{x:α}, x ∈ s → dist x a < δ → f x ≤ M :=
@metric.tendsto_nhds_within_at_top _ (order_dual β) _ _ _ _ _

theorem metric.tendsto_nhds_within_principal {α β : Type*} [metric_space α] {f : α → β} {a : α} {s : set α} {s' : set β} :
  tendsto f (nhds_within a s) (𝓟 s') ↔ ∃ δ > 0, ∀{x:α}, x ∈ s → dist x a < δ → f x ∈ s' :=
by simp [tendsto_principal, metric.eventually_nhds_within]